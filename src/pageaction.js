if (!window.chrome) {
  window.chrome = window.browser;
}

const copyTextToClipboard = (text) => {
  const copyFrom = document.createElement("textarea");
  copyFrom.textContent = text;
  document.body.appendChild(copyFrom);
  copyFrom.select();
  document.execCommand('copy');
  document.body.removeChild(copyFrom);
};

const getSentryURL = async (project, corr_id) => {
  return new Promise((resolve, _) => {
    chrome.runtime.getBackgroundPage(async (w) => {
      let url = await w.getSentryURL(project, corr_id);
      resolve(url);
    });
  });
};

const getLogURL = async (corr_ids) => {
  return new Promise((resolve, _) => {
    chrome.runtime.getBackgroundPage(async (w) => {
      let url = await w.getLogURL(corr_ids);
      resolve(url);
    });
  });
};

const setURL = (id, link, errorText) => {
  if (link) {
    document.getElementById(id).href = link;
    return;
  }

  const surl = document.getElementById(id)
  const parent = surl.parentNode;
  parent.removeChild(surl);
};

const openAll = () => {

    chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
      const currTab = tabs[0];
      if (currTab) {
       chrome.runtime.getBackgroundPage(async (w) => {
           w.openAll(currTab.id);
         });
      }
    });
}

const renderSubRequest = async (details) => {
  if (!details.correlation_id) {
    return;
  }
  document.getElementById('requests').innerHTML += `
<div class="form-group row">
  <div class="col-sm-10">
    <div class="input-group">
      <div class="input-group-prepend">
        <input style="width: 55px;" class="form-control" type="text" readonly value="${details.statusCode}"/>
      </div>
      <input class="form-control" type="text" readonly value="${details.url}"/>
      <!-- input style="width: 50px;" class="form-control" type="text" readonly id="correlation_id_${details.correlation_id}" value="${details.correlation_id}" placeholder="no correlation ID present"/>
      <input style="width: 50px;" class="form-control" type="text" readonly id="cf_ray_id{details.cf_ray_id}" value="${details.cf_ray_id}" placeholder="no CF ray ID present"/ -->
      <div class="input-group-append">
        <a class="btn btn-outline-secondary" target="_blank" id="sentry_url_${details.correlation_id}" href="${await getSentryURL(details.sentry_project, details.correlation_id)}" type="button">Sentry</a>
      </div>
    </div>
  </div>
</div>`
};

const renderInfo = async (data) => {
  let correlation_ids = [];
  let main_id = -1;

  for (let i = 0; i < data.length; i++) {
    if (data[i].correlation_id) {
      correlation_ids.push(data[i].correlation_id);
    }
    if (main_id === -1 && data[i].type === "main_frame") {
      main_id = i;
    } else {
      await renderSubRequest(data[i]);
    }
  }

  if (!data[main_id]) {
    return;
  }

  document.getElementById("correlation_id").value = data[main_id].correlation_id || null;
  document.getElementById("cf_ray_id").value = data[main_id].cf_ray_id || null;
  document.getElementById("loadbalancer").value = data[main_id].loadbalancer || null;
  document.getElementById("status_code").innerHTML = data[main_id].statusCode || null;
  document.getElementById("server").value = data[main_id].server || null;
  document.getElementById("url").value = data[main_id].url || null;

  setURL("sentry_url", data[main_id].sentry_project ? (await getSentryURL(data[main_id].sentry_project, data[main_id].correlation_id)) : null);
  setURL("log_url", data[main_id].sentry_project === "gitlabcom" ? (await getLogURL(correlation_ids)) : null);
};

const copyDebugJson = (data) => {
  const out = {
    main_request: {},
    sub_requests: [],
  };

  for (let i = 0; i < data.length; i++) {
    if(data[i].type === "main_frame") {
      Object.assign(out.main_request, data[i]);
    } else {
      out.sub_requests.push(data[i]);
    }
  }

  copyTextToClipboard(JSON.stringify(out, null, 2));
};

const render = (w, func) => {
  if (!w) {
    return;
  }

  chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
    const currTab = tabs[0];
    if (currTab) {
      func(w.requests[currTab.id]);
    }
  });
}

document.addEventListener('DOMContentLoaded', () => {
  chrome.storage.sync.get({
    notifications_enabled: false,
    notifications_nagging: false,
  }, (items) => {
    console.log(items);
    document.getElementById('notifications_enabled').checked = items.notifications_enabled;
    document.getElementById('notifications_nagging').checked = items.notifications_nagging;

    document.getElementById("notifications_enabled").addEventListener("change", (event) => {
      chrome.storage.sync.set({
        notifications_enabled: event.currentTarget.checked,
      }, (saved) => {
        console.log(saved);
      });
    });
    document.getElementById("notifications_nagging").addEventListener("change", (event) => {
      chrome.storage.sync.set({
        notifications_nagging: event.currentTarget.checked,
      }, (saved) => {
        console.log(saved);
      });
    });
    document.getElementById("all_links").addEventListener("click", openAll);
    document.getElementById("copy_json").addEventListener("click", () => {
      chrome.runtime.getBackgroundPage((w) => {render(w, copyDebugJson)});
    });
   chrome.runtime.getBackgroundPage((w) => {render(w, renderInfo)});
  });
});
